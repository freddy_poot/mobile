# Android Project

Your goal is to create an application that handles the logic to allow the user login and see the main view any time they launch the app(Pokeapi).


## Requirements

-  The application should be built with Java or Kotlin. Please use the Android Studio as IDE.
-  The deliverable should contain complete steps for setting up an environment to run the application. Assume that we have never developed anything on Android.

## User Stories

-  The user should authenticate with email and password (you could use static data and a fake endpoint to validate what the user types on the form.)
-  The user should be able to log out.
-  The user should be able to view a list of pokemons (this is the main view).
-  The user session should be stored locally.
-  The user should be able to see the main view if they had already logged in.
-  The list of pokemons should be stored locally (you don't need to store all of the pokemons returned by the API, just some of them).

## Bonus points

-  Use an architecure to divide and encapsulate components of a feature (e.g. MVC, MVP, MVVM).
-  Create a set of Unit Tests for the project.
-  Create a set of UI Tests for the project.

## Hint

- [Pokemon API Docs](https://pokeapi.co/docsv2/#).
- The Pokeapi has a limit usage per hour. Consider mocking the requests.
- Review the `offset` and `limit` parameters for the GET request.